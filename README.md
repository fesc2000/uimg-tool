This is a tool to extract and create "Intel Unified Binary" images.

It was written to handle firmware images for Intel Puma7 based cable modems.

Usage
=====

Call uimg -h:

~~~
 uimg -u|p|i [-n <name>] uimg-file
   -u   unpack all partitions and write them to write-protected .bin
        files named name_nn[_suffix].bin, where
        - name is the given name or uimg-file (including the path, without
          file extension).
        - nn is the logical partition number.
        - suffix is the name of the partition content, if known by the tool.
   -p   pack all partition files with -n prefix and write to write-protected uimg-file.
        All file names matching the above file format are added as partition.
        The file name can be with content _suffix (default) or without.
   -i   Show info on image file
   -n   name prefix for input/output files (default: input file name without suffix)
~~~

Note that all generated files are write protected to avoid accidental overwrite.
